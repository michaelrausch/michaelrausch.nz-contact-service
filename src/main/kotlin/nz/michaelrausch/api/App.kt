package nz.michaelrausch.api

import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@org.springframework.boot.autoconfigure.SpringBootApplication
class App

fun main(args: Array<String>) {
    org.springframework.boot.SpringApplication.run(nz.michaelrausch.api.App::class.java, *args)
}