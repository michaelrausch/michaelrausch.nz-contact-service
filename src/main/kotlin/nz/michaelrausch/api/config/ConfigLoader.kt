package nz.michaelrausch.api.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File
import java.io.FileNotFoundException

class ConfigLoader {
    /**
     * Load the server config from config.yaml
     */
    fun load(): ConfigProps {
        val mapper = ObjectMapper(YAMLFactory())
        var config: ConfigProps?

        try{
            config = mapper.readValue(File("config.yaml"), ConfigProps::class.java)
        }
        catch(e: Exception){
            throw FileNotFoundException("Couldn't load config.yaml")
        }

        return config
    }
}