package nz.michaelrausch.api.config

data class MailConfig (val api_key: String = "",
                       val to_email: String = "",
                       val subject: String = "")