package nz.michaelrausch.api.util.validation

interface Validator {
    fun validate(): Boolean
}