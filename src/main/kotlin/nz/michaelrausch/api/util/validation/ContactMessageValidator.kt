package nz.michaelrausch.api.util.validation

import nz.michaelrausch.api.dtos.ContactMessageDto
import java.util.regex.Pattern

class ContactMessageValidator(val message: ContactMessageDto) : Validator{
    private val EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

    private fun isValidEmail(email: String) : Boolean {
        val pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE)

        return pattern.matcher(email).matches()
    }

    override fun validate(): Boolean {
        if (!message.message.isNotBlank()) return false
        if (!message.name.isNotBlank()) return false
        if (!isValidEmail(message.fromEmail)) return false

        return true
    }
}