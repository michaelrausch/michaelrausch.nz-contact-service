package nz.michaelrausch.api.util.mail

import com.sendgrid.Content
import com.sendgrid.Email
import com.sendgrid.Mail

abstract class MailBuilder (var subject: String = "",
                            var toEmail: String = "",
                            var fromEmail: String = "",
                            var name: String = "",
                            var content: String = "",
                            var hasPriority: Boolean = false){
   
    fun build(): String {
        return Mail(Email(fromEmail), subject, Email(toEmail), Content("text/html", content)).build()
    }

    fun withFromEmail(email: String): MailBuilder {
        this.fromEmail = email
        return this
    }

    fun withToEmail(email: String): MailBuilder{
        this.toEmail = email
        return this
    }

    fun withSubject(subject: String): MailBuilder{
        this.subject = subject
        return this
    }

    fun withSenderName(name: String): MailBuilder {
        this.name = name
        return this
    }

    fun withPriority(priority: Boolean): MailBuilder {
        this.hasPriority = priority
        return this
    }

    open fun withMessage(message: String): MailBuilder{
        this.content = message
        return this
    }
}