package nz.michaelrausch.api.util.mail

/**
 * Builds the message body for a contact form message
 */
class ContactMailBuilder : MailBuilder() {

    override fun withMessage(message: String): MailBuilder{
        val html = "<h1>Contact Form Message</h1>" +
                "<b>From:</b> $name<br/>" +
                "<b>Email:</b> $fromEmail<br/>" +
                "<b>Message:</b> $message"

        super.content = html
        return this
    }
}