package nz.michaelrausch.api.services

import com.sendgrid.Method
import com.sendgrid.Request
import com.sendgrid.Response
import com.sendgrid.SendGrid
import nz.michaelrausch.api.config.ConfigLoader
import nz.michaelrausch.api.config.ConfigProps
import nz.michaelrausch.api.dtos.ContactMessageDto
import nz.michaelrausch.api.util.mail.MailBuilder
import org.springframework.beans.factory.annotation.Autowired
import java.io.IOException

class MailService {
    private val config: ConfigProps = ConfigLoader().load()
    private val sendgrid: SendGrid

    init {
        sendgrid = SendGrid(config.mail.api_key)
    }

    /**
     * Send an email using the SendGrid API
     *
     * @param message: An initialised MailBuilder
     */
    fun send(message: MailBuilder): Boolean {
        val request = Request()

        try{
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = message.build()

            sendgrid.api(request)
        }
        catch (e: IOException){
            return false
        }

        return true
    }

}