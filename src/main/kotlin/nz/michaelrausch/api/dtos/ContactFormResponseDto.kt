package nz.michaelrausch.api.dtos

data class ContactFormResponseDto(val error: Boolean,
                                  val result: String)