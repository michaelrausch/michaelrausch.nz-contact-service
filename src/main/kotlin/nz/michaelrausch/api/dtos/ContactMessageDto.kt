package nz.michaelrausch.api.dtos

data class ContactMessageDto (val fromEmail: String = "",
                              val name: String = "",
                              val message: String = "")