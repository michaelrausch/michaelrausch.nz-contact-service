package nz.michaelrausch.api.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@RestController
class RootController {
    @GetMapping("/")
    fun root(response: HttpServletResponse): String {
        response.status = 404
        return ""
    }
}