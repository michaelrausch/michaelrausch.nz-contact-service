package nz.michaelrausch.api.controllers

import nz.michaelrausch.api.config.ConfigLoader
import nz.michaelrausch.api.config.ConfigProps
import nz.michaelrausch.api.dtos.ContactMessageDto
import nz.michaelrausch.api.dtos.ContactFormResponseDto
import nz.michaelrausch.api.services.MailService
import nz.michaelrausch.api.util.mail.ContactMailBuilder
import nz.michaelrausch.api.util.mail.MailBuilder
import nz.michaelrausch.api.util.validation.ContactMessageValidator
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@CrossOrigin(origins = arrayOf("*"))
@RestController
class ContactController {
    private val config: ConfigProps = ConfigLoader().load()

    @PostMapping("/contact")
    fun contact(@RequestBody contactMessage: ContactMessageDto): ContactFormResponseDto {
        if (!ContactMessageValidator(contactMessage).validate()) {
            return ContactFormResponseDto(error = true, result = "Validation Error")
        }

        val builder: MailBuilder = ContactMailBuilder()
                .withToEmail(config.mail.to_email)
                .withFromEmail(contactMessage.fromEmail)
                .withSubject(config.mail.subject)
                .withSenderName(contactMessage.name)
                .withMessage(contactMessage.message)

        if(MailService().send(builder)){
            return ContactFormResponseDto(error = false, result = "Message Sent")
        }

        return ContactFormResponseDto(error = true, result = "Failed to send message")
    }
}